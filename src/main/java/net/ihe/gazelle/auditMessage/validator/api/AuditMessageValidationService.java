package net.ihe.gazelle.auditMessage.validator.api;

import java.io.OutputStream;

import net.ihe.gazelle.services.GenericServiceLoader;

public class AuditMessageValidationService {

	private AuditMessageValidationService() {

	}

	private static AuditMessageValidatorProvider getProvider() {
		return GenericServiceLoader.getService(AuditMessageValidatorProvider.class);
	}

	public static String validateAuditMessageAndSaveFormattedReport(String document, String validator,
			OutputStream report, String stylesheetUrl) throws Exception {
		return getProvider().validateAuditMessageAndSaveFormattedReport(document, validator, report, stylesheetUrl);
	}
}
