package net.ihe.gazelle.auditMessage.validator.api;

import java.io.OutputStream;

public interface AuditMessageValidatorProvider {

	public String validateAuditMessageAndSaveFormattedReport(String document, String validator, OutputStream report,
			String stylesheetUrl) throws Exception;
}
